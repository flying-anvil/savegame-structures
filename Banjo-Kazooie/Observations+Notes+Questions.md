# Observations Notes + Questions

- Empty eep has `0x 32 C9 A1 E6` as its last 4 bytes
  - Checksum? If so, how? The other bytes are all `0x00`

- Selecting a Slot for the first time seems to create 4 blocks
  - `0x0000` to `0x0077` | starts with `0x0002`
  - `0x0078` to `0x00EF` | starts with `0x0003`
  - `0x00F0` to `0x0167` | empty (`0x00`)
  - `0x0168` to `0x01DF` | starts with `0x0001`
  - Each is `0x78` (120) bytes long
  - 4 Bytes at the end already filled
    - probably checksum
    - block 3 has empty checksum, probably because all bytes are `0x00`
    - Differ a lot each block
  - Last 4 bytes of whole eep are unchanged
    - No global checksum, only for sections
    - Global checksum decoupled from section, only for global data
  - All Slots are shown as empty still

- Last `0x20` (32) bytes seem to be for global data
  - Stop'n'Swap?

- Slot A seems to be block 3
  - Saving right when control is given
    - Now block 4 is empty (`0x00`), but checksum is still there
    - `$F0` changed to `0x11`
    - `$F1` is now `0x01`
      - Indicates last used save?
    - `$012F` is `0x04`
      - Time spent?
        - Waiting more increases this value
        - Value is the same as statistics/spiral mountain

- Loading a save and saving again seems to swap the order of the blocks
  - A new save is written, old save is still present at block 4
    - block 4 = always backup?
  - Might also happen after some time
    - Entered Lair and waited, then they were swapped again
    - Back at Spiral Mountain they swapped again.

## Levels

### Spiral Mountain

- Accepting the tutorial changes nothing
  - Unlocking Highjump   changes `$E4` flags `00000100`
  - Unlocking Doublejump changes `$E5` flags `10000000`
  - Unlocking Backflip   changes `$E4` flags `00000001`
  - Completing the jump tutorial (only finish the text as of now) changes `$E9` from 3 to 7
    - Story flags?
      - One flag for each completed tutorial, because of multiple steps
        - `00000001` for Highjump
        - `00000010` for Doublejump
        - `00000100` for Backflip
  - Unlocking Swimming/Diving changes `$E4` flags `10000000`
    - Story Flag `$E9` `00001000`
  - Unlocking Climbing changes `$E5` flags `00100000`
    - Story Flag `$E9` `00010000`
  - Unlocking "Beak Barge" changes `$E5` flags `00000001`
    - Story Flag `$E9` `00100000`
  - Unlocking Punch changes `$E5` flags `00010000`
    - Story Flag `$E8` `00001000`
  - Unlocking Roll changes `$E4` flags `00010000`
    - Story Flag `$E8` `00010000`
  - Unlocking Beak attack changes `$E4` flags `00001000`
    - Story Flag `$E8` `00000100`
- Speaking with bottles before the lair
  - Story Flag `$E5` `00001000`
    - Is `$E5` Story Flag or Moveset Flag? Or are they mixed?

- Honey Combs
  - Attack Tutorial:     `$89` `10000000`
  - Jump Tutorial:       `$89` `00001000`
  - Dive Tutorial:       `$89` `00100000`
  - Climb Tutorial:      `$89` `01000000`
  - Beak Barge Tutorial: `$89` `00000001`
  - Waterfall:           `$89` `00010000`

### Grunty's Lair

- Entering
  - Story Flag `$CA` `10000000`
  - Story Flag `$CF` `00100000`
    - One of these for the cutscene, the other for the first text that Grunty says
    - Responsible for spawning in Lair?

- Bottle speaks to you before the 1st level, this seems to be not saved

- Explanation before the puzzle platform
  - Story Flag `$1BC` `10000000`
- Explanation on the puzzle platform
  - Story Flag `$1AA` `01000000`
- Explanation puzzle platform with 1st Jiggy
  - Story Flag `$1AA` `10000000`

---

- Collecting the 1st Jiggy
  - `$170` `00001000` | NPC introduction? Jiggy Flag?
  - `$1D1` `00000001` | Jiggy Flag? Jiggy Count?
  - Both are set at the same time

- Finishing 1st puzzle
  - `$1AD`  `00000001`         | Amount in 1st puzzle?
  - `$1AE`  `00000010`         |   Level unlocked? Grunty taunt text?
  - `$1B3`  `00100000`         |   Level unlocked? Grunty taunt text?
  - `$1D1` ~`00000001` (unset) | Jiggy Count?

- `$AD` For time?
  - It's `$19D` now…
  - Probably starts a byte earlier
    - yes, `$19C` to `$19D`

## NPC introductions

- Empty honey comb:   `$B9` `00001000`
- Regular honey comb: `$B9` `00000100`
- Extra life:         `$B9` `00010000`

## Assumptions

- `$B6`  +  `$B7` => Time in Spiral Mountain (BE)
- `$19C` + `$19D` => Time in Grunty's Lair   (BE)
  - 2 bytes, maybe more

| Offset (0x) | Size (bytes) | Type     | Description                                                           | Comment |
|------------:|-------------:|----------|-----------------------------------------------------------------------|---------|
|             |              |          |                                                                       |         |
|             |              |          |                                                                       |         |
