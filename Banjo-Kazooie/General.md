# General stuff

- 512 byte EEPROM
- 4 sections, each `0x78` (120) bytes long
  - One for each save slot (3) + 1 backup
  - Order is not constant, swaps even mid-game
  - 4-byte checksum at the end of each
  - 2nd byte of each section indicates a slot
    - `0x01` for 1st, `0x02` for 2nd, `0x03` for 3rd, `0x00` for backup
  - 1st byte is `0x11` for last used savegame (_unsure_)
- Last `0x20` (32) bytes for global data
  - Also with last 4 bytes for checksum

The following is valid for each of the 4 sections.
Add `0x78` to the address 1, 2 or 3 times to "select" the section.
`$` denotes address in hexadecimal. Multi-byte types are Big-Endian.
For meanings of flags see `Flags.md`.

| Offset (0x) | Size (bytes) | Type                   | Description                                                           | Comment |
|------------:|-------------:|------------------------|-----------------------------------------------------------------------|---------|
| 0000        |            1 | Byte                   | `0x11` when if this section was used last                             | Unsure if true |
| 0001        |            1 | uint8                  | Slot number, `0x01`, `0x02` and `0x03` for save slot 1, 2 and 3 respectively. `0x0` for backup | Unsure if true |
|             |              |                        |                                                                       |         |
| 0011        |            1 | Flags (Honey Combs #1) | Collected empty honey combs                                           |         |
|             |              |                        |                                                                       |         |
| 003E        |            2 | uint16                 | Time spent in `Spiral Mountain`                                       |         |
|             |              |                        |                                                                       |         |
| 0034        |            2 | uint16                 | Time spent in `Grunty's Lair`                                         |         |
|             |              |                        |                                                                       |         |
| 0041        |            1 | Flags (NPC #1)         | NPC introduction flags                                                |         |
| 0042        |            1 | Flags (Story #4)       |                                                                       |         |
|             |              |                        |                                                                       |         |
| 0054        |            1 | Flags (Story #3)       |                                                                       |         |
|             |              |                        |                                                                       |         |
| 006C        |            1 | Flags (Moves #1)       | Unlocked moves/abilities                                              |         |
| 006D        |            1 | Flags (Moves #2)       | Unlocked moves/abilities                                              |         |
|             |              |                        |                                                                       |         |
| 0070        |            1 | Flags (Story #1)       | Completed tutorial for moves (aka performed them)                     |         |
| 0071        |            1 | Flags (Story #2)       | Completed tutorial for moves (aka performed them)                     |         |
|             |              |                        |                                                                       |         |
| 0074        |            4 | uint32                 | Checksum for the section (unknown how to calculate)                   |         |
|             |              |                        |                                                                       |         |
|             |              |                        |                                                                       |         |
|             |              |                        |                                                                       |         |
|             |              |                        |                                                                       |         |
|             |              |                        |                                                                       |         |
|             |              |                        |                                                                       |         |
|             |              |                        |                                                                       |         |
