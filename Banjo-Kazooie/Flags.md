# Flags

## Honey Combs #1

Offset `0x11`

| Bit mask | Meaning                     | Comment |
|---------:|-----------------------------|---------|
|`00000001`| Spiral Mountain (Rock)      |         |
|`00000010`|                             |         |
|`00000100`|                             |         |
|`00001000`| Spiral Mountain (Jump)      |         |
|`00010000`| Spiral Mountain (Waterfall) |         |
|`00100000`| Spiral Mountain (Dive)      |         |
|`01000000`| Spiral Mountain (Climb)     |         |
|`10000000`| Spiral Mountain (Attack)    |         |

---

## NPC #1

Offset `0x41`

| Bit mask | Meaning              | Comment |
|---------:|----------------------|---------|
|`00000001`|                      |         |
|`00000010`|                      |         |
|`00000100`| Regular honey comb   |         |
|`00001000`| Empty honey comb     |         |
|`00010000`| Extra life           |         |
|`00100000`|                      |         |
|`01000000`|                      |         |
|`10000000`|                      |         |

---

## Moves #1

Offset `0x6C`

| Bit mask | Meaning              | Buttons       | Comment |
|---------:|----------------------|---------------|---------|
|`00000001`| Backflip             | `Z` + `A`     |         |
|`00000010`|                      |               |         |
|`00000100`| High jump            | `A` (hold)    | Regular jump, but higher |
|`00001000`| Beak attack          | `A` + `B`     |         |
|`00010000`| Roll attack          | `Stick` + `B` |         |
|`00100000`|                      |               |         |
|`01000000`|                      |               |         |
|`10000000`| Diving               | `B`           |         |

---

## Moves #2

Offset `0x6D`

| Bit mask | Meaning              | Buttons       | Comment |
|---------:|----------------------|---------------|---------|
|`00000001`| Beak Barge           | `Z` + `B`     |         |
|`00000010`|                      |               |         |
|`00000100`|                      |               |         |
|`00001000`| Tutorial complete    |               | Speaking with Bottles at the bridge |
|`00010000`| Punch                | `B`           |         |
|`00100000`| Climbing             | `A` to climb  |         |
|`01000000`|                      |               |         |
|`10000000`| Double/Flutter jump  | `A` + `A`     |         |

---

## Story #4

Offset `0x42`

| Bit mask | Meaning                | Comment |
|---------:|------------------------|---------|
|`00000001`|                        |         |
|`00000010`|                        |         |
|`00000100`|                        |         |
|`00001000`|                        |         |
|`00010000`|                        |         |
|`00100000`|                        |         |
|`01000000`| Puzzle platform tutorial (when stepping on it) |         |
|`10000000`| Puzzle platform with 1st Jiggy                 |         |

---

## Story #3

Offset `0x54`

| Bit mask | Meaning                | Comment |
|---------:|------------------------|---------|
|`00000001`|                        |         |
|`00000010`|                        |         |
|`00000100`|                        |         |
|`00001000`|                        |         |
|`00010000`|                        |         |
|`00100000`|                        |         |
|`01000000`|                        |         |
|`10000000`| Puzzle platform explanation (before stepping on it) |         |

---

## Story #1

Offset `0x70`

| Bit mask | Meaning                | Comment |
|---------:|------------------------|---------|
|`00000001`|                        |         |
|`00000010`|                        |         |
|`00000100`| Performed Beak attack  |         |
|`00001000`| Performed Punch attack |         |
|`00010000`| Performed Roll attack  |         |
|`00100000`|                        |         |
|`01000000`|                        |         |
|`10000000`|                        |         |

---

## Story #2

Offset `0x71`

| Bit mask | Meaning                    | Comment |
|---------:|----------------------------|---------|
|`00000001`| Performed Highjump         |         |
|`00000010`| Performed Doublejump       |         |
|`00000100`| Performed Backflip         |         |
|`00001000`| Performed Swimming/Diving  |         |
|`00010000`| Performed Climbing         |         |
|`00100000`| Performed Beak Barge       |         |
|`01000000`|                            |         |
|`10000000`|                            |         |

---

## Template

Offset `0x00`

| Bit mask | Meaning              | Comment |
|---------:|----------------------|---------|
|`00000001`|                      |         |
|`00000010`|                      |         |
|`00000100`|                      |         |
|`00001000`|                      |         |
|`00010000`|                      |         |
|`00100000`|                      |         |
|`01000000`|                      |         |
|`10000000`|                      |         |

---
