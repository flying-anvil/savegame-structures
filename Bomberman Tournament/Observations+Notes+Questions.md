# Observations Notes + Questions

- Fresh save in A fills 1st half with data, seconds halt with `ÿ` (`0xFF`)
- Last 4 bytes from A are `0xFF`, last 4 bytes from B is `0x01 00 00 00`
  - Are the last 4 bytes of the slots swapped?
  - Creating B leaves last 4 bytes of A as `0xFF`, but changes last 4 bytes of B to `0x02 00 00 00`
    - Having only B still makes them `0x02 00 00 00`
      - Creating A makes it `0x01 00 00 00` again
        - Last saved slot (the game automatically selects the marked slot)
        - Does that make the last 4 bytes of A unused?

- Saving without doing anything (except waiting) causes some bytes to always change
  - `0x1150`
  - `0x1420`
  - `0x2896`
  - `0x289E`
  - `0x28A2`
  - `0x28AA`
  - `0x28AE`
  - `0x28B6`
  - `0x3CAE`
  - `0x3CB0`
  - After moving (changing some bytes) all of `0x3CAE` to `0x3cB1` changed
    - Checksum?

- Moving down changes `0x31` and `0x3D`, they seem to be the same
  - Moving down more also changes `0x30` and `0x3C`
  - Cannot easily change them (checksum?)

- Looking up changes `0x20` from `0` to `2`

- Moving slightly to the right changes `0x20`
  - Facing direction?

- Moving slightly to the right changes [`0x1140` to `0x1141`] and [`0x1144` to `0x1145`]
  - Also changes `0x114C`, `0x114E`, `0x141B`

- Moving right, faing right changes `0x20` to `1`

- Moving left, [`0x2C` to `0x2D`] and [`0x38` to `0x39`] changed
  - Same value at both offsets

- Scrolling does not seem to make any difference, still many unknown bytes

- Changing maps (according to save) changes `0x3CBA`

- It saves all kind of crap
  - IFrames
  - State of destructables
  - Dropped powerups
  - Obstacle state (like vines in L.Forest)
  - Probably enemy state

- 

## Unknown/Unclear

- `0x114A`

## Assumptions

- `0x31` and `0x3D` => Y-Position
  - 2 bytes, maybe uint16 (LE)
- `0x2C` and `0x38` => X-Position
  - 2 bytes, maybe uint16 (LE)
- `0x20` => Facing diraction
  - `0` => down
  - `1` => right
  - `2` => up
  - `3` => left
  - `4` => up-right
  - `5` => down-right
  - `6` => down-left
  - `7` => up-left
- `0x3CBA` => Map
  - `0F` => Alpha
  - `0B` => L.Forest
- 
- 
- 
- 

