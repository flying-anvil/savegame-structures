# General stuff

- 32kb in size
- Save file is split in two parts, first half for slot 1 (A), seconds half for slot 2 (B)
- Signature at top `BOMBERMANCHECKER$S` (`0x42 4F 4D 42 45 52 4D 41 4E 43 48 45 43 4B 45 52 80 53`)
  - Both A and B contain this check
- 
- 
- 
- 
- 
- 


| Offset (0x) | Size (bytes) | Type     | Description                                                           | Comment |
|------------:|-------------:|----------|-------------------------------------------------------------|---------|
| 0x0000      |           18 | ASCII    | Constant value `BOMBERMANCHECKER$S`                                   |         |
|             |              |          |                                                                       |         |
| 0x0020      |            1 | int8     | Facing direction <br/> `0` = down <br/> `1` = right <br/> `2` = up <br/> `3` = left <br/> `4` = up-right <br/> `5` = down-right <br/> `6` = down-left <br/> `7` = up-left |         |
|             |              |          |                                                                       |         |
|             |              |          |                                                                       |         |
| 0x7FFC      |            4 | int32    | Last saved slot, either `1` (A) or `2` (B)                                | Can easily be changed. When forced to empty slot, defaults to used slot |
