# Savegame structures of games where I couldn't find documentation for

| Game                    | State         |
|-------------------------|---------------|
| Bomberman Tournament    | Very basic. Game dumps a lot of unnecessary stuff into the save :( |
| Banjo-Kazooie           | Early, some flags + values. Pretty simple to figure out            |
|                         |               |
|                         |               |
